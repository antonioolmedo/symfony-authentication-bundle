<?php

namespace AOlmedo\AuthenticationBundle\Security\Voter;

use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Http\AccessMap;
use Symfony\Component\HttpFoundation\Request;

class ResourceRolePermissionVoter implements VoterInterface
{

    protected $entityManager;
    protected $accessMap;

    /**
     * Constructor.
     *
     * @param string $prefix The role prefix
     */
    public function __construct(EntityManager $entityManager, AccessMap $accessMap){
        $this->entityManager = $entityManager;
        $this->accessMap = $accessMap;
    }

    public function vote(TokenInterface $token, $subject, array $attributes){
        // abstain vote by default in case none of the attributes are supported
        $vote = self::ACCESS_ABSTAIN;

        if ($this->voteOnAttributes($attributes, $subject, $token)) {
                return self::ACCESS_GRANTED;
        }else{
            return self::ACCESS_DENIED;
        }
    }

    protected function voteOnAttributes($attributes, $subject, TokenInterface $token){
         if($subject instanceof Request){
            $granted = false;
            $roles = $this->accessMap->getPatterns($subject)[0];
            foreach ($attributes as $key => $attribute) {
                if(in_array($attribute, $roles) && $attribute === 'IS_AUTHENTICATED_ANONYMOUSLY'){
                    $granted = true;
                }elseif(in_array($attribute, $roles) && $attribute === 'IS_AUTHENTICATED_FULLY' && $token instanceof Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken){
                    $granted = true;
                }else{
                    $granted = false;
                    continue;
                }
            }
            return $granted;
        }

        $roles = array();
        foreach ($token->getRoles() as $key => $role) {
            $roles[] = $role->getRole();
        }

        $result = $this->entityManager->getRepository('AOlmedo\AuthenticationBundle\Entity\ResourcePermissionRole')->getResourcePermissionRole($subject, $attributes, $roles);

        $total = count($result);
        if(count($attributes) === $total  && $total > 0){
            return true;
        }else{
            return false;
        }
    }

}
