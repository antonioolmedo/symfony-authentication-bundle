<?php

namespace AOlmedo\AuthenticationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AuthenticationController extends Controller {

    public function loginAction(Request $request) {
        //	TODO implementar errores login
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render($request->attributes->get('template'), array(
            // last username entered by the user
            'last_username' => $lastUsername,
            'error' => $error
        ));
    }

}
