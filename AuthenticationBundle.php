<?php

namespace AOlmedo\AuthenticationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use AOlmedo\AuthenticationBundle\DependencyInjection\AuthenticationExtension;

class AuthenticationBundle extends Bundle
{
    public function getContainerExtension(){
        return new AuthenticationExtension();
    }
}
