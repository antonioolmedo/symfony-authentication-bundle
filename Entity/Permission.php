<?php

namespace AOlmedo\AuthenticationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AOlmedo\AuthenticationBundle\Entity\Repository\PermissionRepository")
 * @ORM\Table(name="aolmedo_auth_permissions")
 */
class Permission
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @ORM\Column(type="string", unique=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="ResourcePermission", mappedBy="permission")
     */
    private $resourcesPermissions;


    public function __construct(){}

    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function getResourcesPermissions(){
        return $this->resourcesPermissions;
    }

}
