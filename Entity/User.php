<?php

namespace AOlmedo\AuthenticationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use \Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Table(name="aolmedo_auth_users", options={"comment":"Tabla de usuarios"})
 * @ORM\Entity(repositoryClass="AOlmedo\AuthenticationBundle\Entity\Repository\UserRepository")
 */
class User implements AdvancedUserInterface, \Serializable
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $surname;

    /**
     * @ORM\Column(name="second_surname", type="string", length=50, nullable=true)
     */
    private $secondSurname;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=60, unique=true)
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity="UserRole", mappedBy="user")
     */
    private $userRoles;

    /**
     * @ORM\Column(name="active", type="boolean", nullable = false)
     */
    private $active;

    public function __construct(){
        $this->active = true;
    }

    public function getId(){
        return $this->id;
    }

    public function getUsername(){
        return $this->username;
    }

    public function getName(){
        return $this->name;
    }

    public function getSurname(){
        return $this->surname;
    }

    public function getSecondSurname(){
        return $this->secondSurname;
    }

    public function getEmail(){
        return $this->email;
    }

    public function getSalt(){
        return null;
    }

    public function getPassword(){
        return $this->password;
    }

    public function getRoles(){
        $role = empty($this->userRoles) ? null : $this->userRoles->get(0);
        if(empty($role)){
            return array();
        }else{
            return array($role->getRole());
        }
    }

    public function getUserRole(){
        $userRole = empty($this->userRoles) ? null : $this->userRoles->get(0);
        return $userRole;
    }

    public function getUserRoles(){
        return $this->userRoles;
    }

    public function getRole(){
        $role = empty($this->userRoles) ? null : $this->userRoles->get(0);
        if(empty($role)){
            return null;
        }else{
            return $role->getRole();
        }
    }

    public function isAccountNonExpired(){
        return true;
    }

    public function isAccountNonLocked(){
        return true;
    }

    public function isCredentialsNonExpired(){
        return true;
    }

    public function isActive(){
        return $this->isEnabled();
    }

    public function isEnabled(){
        return $this->active;
    }

    public function eraseCredentials(){}

    public function setUsername($username){
        $this->username = $username;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function setSurname($surname){
        $this->surname = $surname;
    }

    public function setSecondSurname($secondSurname){
        $this->secondSurname = $secondSurname;
    }

    public function setPassword($password){
        $this->password = $password;
    }

    public function setEmail($email){
        $this->email = $email;
    }

    public function setActive($active){
        $this->active = $active;
    }

    public function setProductivity($productivity){
        $this->productivity = $productivity;
    }

    public function setRole($role){
        $arrCollection = new ArrayCollection();
        $arrCollection->add($role);
        $this->role = $arrCollection;
    }

    public function getCompany(){
        return $this->company;
    }

    public function setCompany(CompanyEntity $company){
        $this->company = $company;
    }


    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->active,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized){
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->active,
        ) = unserialize($serialized);
    }
}
