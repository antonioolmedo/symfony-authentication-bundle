<?php

namespace AOlmedo\AuthenticationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AOlmedo\AuthenticationBundle\Entity\Repository\ResourcePermissionRepository")
 * @ORM\Table(name="aolmedo_auth_resources_permissions")
 */
class ResourcePermission
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Resource", inversedBy="resourcesRolesPermissions")
     * @ORM\JoinColumn(name="fk_id_resource", referencedColumnName="id", nullable=false)
     */
    private $resource;

    /**
     * @ORM\ManyToOne(targetEntity="Permission", inversedBy="resourcesRolesPermissions")
     * @ORM\JoinColumn(name="fk_id_permission", referencedColumnName="id", nullable=false)
     */
    private $permission;

    public function __construct(){}

    public function getPermission(){
        return $this->permission;
    }

    public function setPermission($permission){
        $this->permission = $permission;
    }

    public function getResource(){
        return $this->resource;
    }

    public function setResource($resource){
        $this->resource = $resource;
    }

}
