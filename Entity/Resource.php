<?php

namespace AOlmedo\AuthenticationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AOlmedo\AuthenticationBundle\Entity\Repository\ResourceRepository")
 * @ORM\Table(name="aolmedo_auth_resources")
 */
class Resource
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @ORM\Column(type="string", unique=true)
     */
    private $name;

    /**
    * @ORM\OneToMany(targetEntity="ResourcePermission", mappedBy="resource")
    */
    private $resourcesPermissions;

    public function __construct(){}

    public function getName(){
        return $this->name;
    }

    public function getResourcesPermissions(){
        return $this->resourcesPermissions;
    }

}
