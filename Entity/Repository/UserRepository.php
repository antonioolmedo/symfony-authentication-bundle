<?php

namespace AOlmedo\AuthenticationBundle\Entity\Repository;

use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository implements UserLoaderInterface
{

    public function getUsersByIds($ids){
        return $this->getEntityManager()->createQueryBuilder()
            ->select('u.id, u.username')
            ->from('AOlmedo\AuthenticationBundle\Entity\UserEntity', 'u', 'u.id')
            ->where('u.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    public function loadUserByUsername($username){
        return $this->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getUsersLikeUsername($username){
    	return $this->createQueryBuilder('u')
    		->select('partial u.{id,username,name,surname,secondSurname,email,active}')
            ->where('u.username LIKE :username')
            ->setParameter('username', $username.'%')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

}
