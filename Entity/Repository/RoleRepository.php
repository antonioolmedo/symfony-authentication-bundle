<?php

namespace AOlmedo\AuthenticationBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class RoleRepository extends EntityRepository
{
    public function getRoles(){
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('r.id AS id, r.name AS name')
          ->from('AOlmedo\AuthenticationBundle\Entity\Role', 'r', 'r.name')
          ->groupBy('r.id');
        $query = $qb->getQuery();
        return $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    public function getRolesLikeName($name){
    	return $this->createQueryBuilder('u')
    		->select('partial u.{id,name}')
            ->where('u.name LIKE :name')
            ->setParameter('name', $name.'%')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
}
