<?php

namespace AOlmedo\AuthenticationBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class UserRoleRepository extends EntityRepository
{
    public function getUserRolesByUser($user){
    	return $this->createQueryBuilder('u')
    		->select('r.id AS role_id, r.name AS role_name, us.id AS user_id')
            ->leftJoin('u.role', 'r')
            ->leftJoin('u.user', 'us')
            ->where('IDENTITY(u.user) LIKE :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

}
