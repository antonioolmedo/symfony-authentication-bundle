<?php

namespace AOlmedo\AuthenticationBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class ResourcePermissionRepository extends EntityRepository
{
    public function getResourcesPermissions($resource = null){
        $data = array();
        $qb = $this->createQueryBuilder('rp')
            ->select('res.id AS resource_id, res.name AS resource_name, pe.id AS permission_id, pe.name AS permission_name, pe.icon AS permission_icon, pe.color AS permission_color')
            ->leftJoin('rp.resource', 'res')
            ->leftJoin('rp.permission', 'pe');

        if(!empty($resource)){
            $qb->where('res.id LIKE :resource')->setParameter('resource', $resource);
        }
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        foreach ($result as $key => $value) {
            $data[$value['resource_name']]['resource_id'] = $value['resource_id'];
            $data[$value['resource_name']]['resource_name'] = $value['resource_name'];
            $data[$value['resource_name']]['permissions'][$value['permission_name']] = array(
                'id' => $value['permission_id'],
                'name' => $value['permission_name'],
                'icon' => $value['permission_icon'],
                'color' => $value['permission_color']
            );
        }
        return $data;
    }
}
