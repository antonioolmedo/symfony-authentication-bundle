<?php

namespace AOlmedo\AuthenticationBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class ResourceRepository extends EntityRepository
{
    public function getResources(){
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('res.id AS id, res.name AS name, res.description AS description, res.icon AS icon')
          ->from('AOlmedo\AuthenticationBundle\Entity\Resource', 'res', 'res.name')
          ->groupBy('res.id');
        $query = $qb->getQuery();
        return $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
}
