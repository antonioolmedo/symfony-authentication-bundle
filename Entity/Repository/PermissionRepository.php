<?php

namespace AOlmedo\AuthenticationBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class PermissionRepository extends EntityRepository
{
    public function getPermissions(){
        $qb = $this->createQueryBuilder('p');
        $query = $qb->getQuery();
        return $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
}