<?php

namespace AOlmedo\AuthenticationBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class ResourcePermissionRoleRepository extends EntityRepository
{
    public function getAccessibleResources($userRole, $permission = 'access'){
        $qb = $this->createQueryBuilder('rpr');
        $qb->select('IDENTITY(rp.resource) AS resource_id')
           ->leftJoin('rpr.resourcePermission', 'rp')
           ->leftJoin('rpr.role', 'r')
           ->leftJoin('rp.permission', 'permission')
           ->where('r.name LIKE :role AND permission.name LIKE :permission')
           ->setParameter('role', empty($userRole) ? null : $userRole->getName())
           ->setParameter('permission', $permission)
           ->groupBy('rp.resource');
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        $return = array();
        foreach ($result as $key => $value) {
            $return[] = $value['resource_id'];
        }
        return $return;
    }

    public function getResourcePermissionRole($resource, $permissions, $roles){
        $qb = $this->createQueryBuilder('rpr');
        $qb->select(array('ro.name', 're.name AS resource', 'pe.name AS permission'))
           ->leftJoin('rpr.role', 'ro')
           ->leftJoin('rpr.resourcePermission', 'rp')
           ->leftJoin('rp.permission', 'pe')
           ->leftJoin('rp.resource', 're')
           ->where('ro.name IN (:roles) AND re.name LIKE :resource AND pe.name IN (:permission)')
           ->setParameter('roles', $roles)
           ->setParameter('resource', $resource)
           ->setParameter('permission', $permissions);
        $query = $qb->getQuery();
        return $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    public function getResourcesPermissionsRoles(){
        $qb = $this->createQueryBuilder('rpr');
        $qb->select('res.id AS resource_id, res.name AS resource_name, res.description AS resource_description, res.icon AS resource_icon, r.id AS role_id, r.name AS role_name, p.id AS permission_id, p.name AS permission_name')
           ->leftJoin('rpr.resourcePermission', 'rp')
           ->leftJoin('rpr.role', 'r')
           ->leftJoin('rp.permission', 'p')
           ->leftJoin('rp.resource', 'res');
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        $resources = $this->getEntityManager()->getRepository('AOlmedo\AuthenticationBundle\Entity\Resource')->getResources();
        $roles = $this->getEntityManager()->getRepository('AOlmedo\AuthenticationBundle\Entity\Role')->getRoles();
        $resourcesPermissions = $this->getEntityManager()->getRepository('AOlmedo\AuthenticationBundle\Entity\ResourcePermission')->getResourcesPermissions();

        $return = array();

        foreach ($resources as $key => $value) {
            foreach ($roles as $roleName => $role) {
                $roles[$roleName]['permissions'] = array();
                if(array_key_exists($key, $resourcesPermissions)){
                    $roles[$roleName]['permissions'] = $resourcesPermissions[$key]['permissions'];
                }
            }

            $return[$value['name']] = array(
                'id' => $value['id'],
                'name' => $value['name'],
                'description' => $value['description'],
                'icon' => $value['icon'],
                'roles' => $roles
            );
        }
    
        //  añadimos los permisos asignados de ese recurso
        foreach ($result as $key => $value) {
            if(!empty($value['resource_name'])){
                $return[$value['resource_name']]['roles'][$value['role_name']]['permissions'][$value['permission_name']]['assigned'] = true;
            }
        }

        return $return;
    }
}
