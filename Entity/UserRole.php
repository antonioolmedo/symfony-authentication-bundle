<?php

namespace AOlmedo\AuthenticationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AOlmedo\AuthenticationBundle\Entity\Repository\UserRoleRepository")
 * @ORM\Table(name="aolmedo_auth_users_roles")
 */
class UserRole
{

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="User", inversedBy="id")
     * @ORM\JoinColumn(name="fk_id_user", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Role", inversedBy="resourcesRolesPermissions")
     * @ORM\JoinColumn(name="fk_id_role", referencedColumnName="id", nullable=false)
     */
    private $role;


    public function __construct(){}


    public function getUser(){
        return $this->user;
    }

    public function setUser(UserEntity $user){
        $this->user = $user;
    }

    public function getRole(){
        return $this->role;
    }

    public function setRole(RoleEntity $role){
        $this->role = $role;
    }

}
