<?php

namespace AOlmedo\AuthenticationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AOlmedo\AuthenticationBundle\Entity\Repository\ResourcePermissionRoleRepository")
 * @ORM\Table(name="aolmedo_auth_resources_permissions_roles")
 */
class ResourcePermissionRole
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="ResourcePermission", inversedBy="id")
     * @ORM\JoinColumn(name="fk_id_resource_permission", referencedColumnName="id", nullable=false)
     */
    private $resourcePermission;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Role", inversedBy="resourcesRolesPermissions")
     * @ORM\JoinColumn(name="fk_id_role", referencedColumnName="id", nullable=false)
     */
    private $role;

    public function __construct(){}


    public function getResourcePermission(){
        return $this->resourcePermission;
    }

    public function setResourcePermission($resourcePermission){
        $this->resourcePermission = $resourcePermission;
    }

    public function getRole(){
        return $this->role;
    }

    public function setRole($role){
        $this->role = $role;
    }

}
