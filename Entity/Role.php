<?php

namespace AOlmedo\AuthenticationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\RoleInterface;

/**
 * @ORM\Entity(repositoryClass="AOlmedo\AuthenticationBundle\Entity\Repository\RoleRepository")
 * @ORM\Table(name="aolmedo_auth_roles")
 */
class Role implements RoleInterface
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="ResourcePermissionRole", mappedBy="role")
     */
    private $resourcesPermissionsRoles;

    public function __construct(){}

    public function getId(){
        return $this->id;
    }

    public function getRole(){
        return $this->name;
    }

    public function getName(){
        return $this->name;
    }

    public function getResourcesPermissionsRoles(){
        return $this->resourcesPermissionsRoles;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function __toString() {
        return $this->name;
    }
}
